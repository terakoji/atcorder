#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
https://tdpc.contest.atcoder.jp/tasks/tdpc_contest
"""

import sys
import math
from inspect import currentframe

def debug_print(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

K = int(input())
R = [int(input()) for _ in range(2**K)]

# round r (1<K), で候補 2**k 自分がiの時の候補者のリストを返す
def candidate_generator(r, k, i):
    r -= 1
    base = (2**r ^ i) >> r << r
    return range(base, base + 2**r)
    
dp = [[1.0] * 2**K for _ in range(K + 1)]

for r in range(1, K + 1):
    for i in range(2**K):
        p_candidates_lose = 0
        for candidate in candidate_generator(r, K, i):
            p_candidates_lose += dp[r - 1][candidate] / (1 + 10 ** ((R[candidate] - R[i]) / 400.0))
        dp[r][i] = dp[r - 1][i] * p_candidates_lose

for i in range(2**K):
    print(dp[K][i])

    
    
