#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
https://tdpc.contest.atcoder.jp/tasks/tdpc_contest
"""

import sys
import math
from inspect import currentframe
from collections import defaultdict

def debug_print(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

N, D = map(int, input().split())

def factrize(num):
    factor2 = 0
    factor3 = 0
    factor5 = 0
    while num % 2 == 0:
        num /= 2
        factor2 += 1
    while num % 3 == 0:
        num /= 3
        factor3 += 1
    while num % 5 == 0:
        num /= 5
        factor5 += 1
    if num == 1:
        return (factor2, factor3, factor5)
    else:
        return None

factors = factrize(D)

if factors == None:
    print(0.0)
    exit()

(factor2, factor3, factor5) = factors

dp = defaultdict(lambda: 0.0)
dp[(0,0,0,0)] = 1.0
dice_p = 1.0 / 6.0

for n in range(N):
    for i in range(factor2 + 1):
        for j in range(factor3 + 1):
            for k in range(factor5 + 1):
                dp[(n + 1, i, j, k)] += dice_p * dp[(n, i, j, k)]
                dp[(n + 1, i + 1, j, k)] += dice_p * dp[(n, i, j, k)]
                dp[(n + 1, i, j + 1, k)] += dice_p * dp[(n, i, j, k)]
                dp[(n + 1, i + 2, j, k)] += dice_p * dp[(n, i, j, k)]
                dp[(n + 1, i, j, k + 1)] += dice_p * dp[(n, i, j, k)]
                dp[(n + 1, i + 1, j + 1, k)] += dice_p * dp[(n, i, j, k)]
debug_print(dp[(0,0,0,0)])
print(dp[N, factor2, factor3, factor5])                
