#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
https://tdpc.contest.atcoder.jp/tasks/tdpc_contest
"""

import sys
import math
from inspect import currentframe

def debug_print(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

N = int(input())
P = list(map(int, input().split()))
max_p = sum(P)

# 今みる問題iで得点jが作れるか？
dp = [[False for j in range(max_p + 1)] for i in range(N + 1)]
dp[0][0] = True


for i in range(N):
    for j in range(max_p + 1):
        if P[i] > j:
            dp[i + 1][j] = dp[i][j]
        else:
            dp[i + 1][j] = dp[i][j - P[i]] or dp[i][j]
    #debug_print(dp)
            
print(dp[N].count(True))
