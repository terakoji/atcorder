#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
https://tdpc.contest.atcoder.jp/tasks/tdpc_contest
"""

import sys
import math
from inspect import currentframe

def debug_print(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

A, B = map(int, input().split())
a = list(map(int, input().split()))
b = list(map(int, input().split()))

# Aにi, Bにj残っている際の先手が取る価値の合計 dp[i][j]
dp = [[0 for j in range(B + 1)] for i in range(A + 1)]

# 手番か判定
def is_move(i, j):
    return (A +  B - i - j) % 2 == 0

for i in range(0, A + 1):
    for j in range(0, B + 1):
        if i == 0 and j == 0:
            dp[0][0] = 0
        elif j == 0:
            if is_move(i, j):
                dp[i][0] = dp[i - 1][0] + a[A - i]
            else:
                dp[i][0] = dp[i - 1][0]
        elif i == 0:
            if is_move(i, j):
                dp[0][j] = dp[0][j - 1] + b[B - j]
            else:
                dp[0][j] = dp[0][j - 1]
        else:
            if is_move(i, j):
                dp[i][j] = max(dp[i - 1][j] + a[A - i], dp[i][j - 1] + b[B - j])
            else:
                dp[i][j] = min(dp[i - 1][j], dp[i][j - 1])
        debug_print(dp)                

print(dp[A][B])
    
