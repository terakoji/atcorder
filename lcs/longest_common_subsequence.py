#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from inspect import currentframe
def chkprint(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

q = int(input())
dataset = [(input(), input()) for x in range(q)]

#chkprint(dataset)

def get_lcs(x, y):
    # LCS to i,j
    dp = {}
    dp.update({(i, 0):0 for i in range(len(x) + 1)})
    dp.update({(0, j):0 for j in range(len(y) + 1)})

    for i in range(1, len(x) + 1):
        for j in range(1, len(y) + 1):
            if x[i - 1] == y[j - 1]:
                dp[(i, j)] = dp[(i - 1, j - 1)] + 1
            else:
                dp[(i, j)] = max(dp[(i - 1, j)], dp[(i, j - 1)])
    return dp[(len(x), len(y))]

for data in dataset:
    print(get_lcs(data[0], data[1]))
