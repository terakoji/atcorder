#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys

def printerr(value):
    print(value, file=sys.stderr) 

N, K = map(int, input().split())
S = input()
S_list = list(S)
S_list.sort()
S_sorted = ''.join(S_list)
printerr("soreted = {}".format(S_sorted))

u"""２つの文字列の不一致の数を返す"""
def calc_str_distance(str1, str2):
    count = 0
    for i in range(len(str1)):
        if str1[i] != str2[i]:
            count += 1
    return count

u"""２つの文字列のなかでstr2に含まれないstr1の要素の数を返す"""
def calc_non_common(str1, str2):
    str2_list = list(str2)
    for c in str1:
        if c in str2_list:
            str2_list.remove(c)
    return len(str2_list)

stop = 0

for i in range(1, N):
    stop = i
    printerr("i={}".format(i))
    frontward_distance = calc_str_distance(S[0:i], S_sorted[0:i])
    backward_distance = calc_non_common(S_sorted[i:], S[i:])
    printerr("frontward dist = {}".format(frontward_distance))
    printerr("backward dist = {}".format(backward_distance))
    if frontward_distance + backward_distance > K:
        stop = i - 1
        break

printerr("stop = {}".format(stop))
S_list = list(S)
for i, swap_char in enumerate(S_sorted[0:stop]):
    swap_char_ind = "".join(S_list[i:]).find(swap_char) + i
    printerr("swap {} {}".format(i, swap_char_ind))
    S_list[swap_char_ind], S_list[i] = S_list[i], S_list[swap_char_ind]
    
print("".join(S_list))
