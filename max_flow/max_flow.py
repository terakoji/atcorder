#!/usr/bin/python3
# -*- coding: utf-8 -*-


u"""
最大流問題 蟻本の写経
"""
# Graph = [[Edge_from_v0_0, Edge_from_v0_1, ...], []]

class Ford_Fulkerson:
    def __init__(self, N):
        self.used = [False] * (N + 1)
        self.G = [[0] * (N + 1) for i in range(N + 1)]
        self.N = N
 
    def add_edge(self, fr, to, cap):
        self.G[fr][to] = cap
        # self.G[to][fr] = cap
 
    def dfs(self, v, t, f):
        if v == t:
            return f
 
        self.used[v] = True
        for i in range(self.N + 1):
            if v == i:
                continue
            if (not self.used[i]) and self.G[v][i] > 0:
                d = self.dfs(i, t, min(f, self.G[v][i]))
                if d > 0:
                    self.G[v][i] -= d
                    self.G[i][v] += d
                    return d
        return 0
 
    def max_flow(self):
        flow = 0
        while True:
            self.used = [False] * (self.N + 1)
            f = self.dfs(0, self.N, 10 ** 9)
            if f == 0:
                break
            flow += f
 
        return flow



