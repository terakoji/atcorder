#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from inspect import currentframe
from max_flow import Edge
from max_flow import MaxFlow
from max_flow import add_bi_edge
from max_flow import add_edge

def debug_print(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

sys.setrecursionlimit(10000)    

num_vertex = 7 
graph = [[] for _ in range(num_vertex)]
add_bi_edge(graph, 0, 1, 1)
add_bi_edge(graph, 0, 2, 1)
add_bi_edge(graph, 1, 3, 1)
add_bi_edge(graph, 3, 4, 1)
add_bi_edge(graph, 3, 5, 1)
add_edge(graph, 4, 6, 1)
add_edge(graph, 5, 6, 1)

debug_print(graph)

max_flow = MaxFlow(graph)
flow = max_flow.calc_max_flow(0, 6)

debug_print(flow)


