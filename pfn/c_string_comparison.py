#!/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np

S = input()
T = input()

# 完全一致でない文字列同士が置換ができるか調べる関数
def can_replace(s1, s2):
    ns1 = np.array(list(map(ord, s1)))
    ns2 = np.array(list(map(ord, s2)))    
    return np.sum(ns1 != ns2) < 2
        
# 挿入と削除はSとTを入れ替えれば同じ
def can_insert(short, long):
    for i, c in enumerate(short):
        if c != long[i]:
            return short[i:] == long[i + 1:]
    return True

can_match = False

if len(S) == len(T) and can_replace(S, T):
    print('YES')
elif len(S) +1 == len(T) and can_insert(S, T):
    print('YES')
elif len(S) == len(T) + 1 and can_insert(T, S):
    print('YES')
else:
    print('NO')


    
    

    
        
        

    







