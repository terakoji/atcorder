#!/usr/bin/python3
# -*- coding: utf-8 -*-

from collections import Counter
from string import ascii_lowercase
import sys
import math
from inspect import currentframe

def debug_print(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)


S = input()

counter = Counter(S)
BIG_NUMBER = 1000000007 

# 組み合わせの数 Pascalの三角形でiCjを計算しておく
comb = [[0] * 512 for _ in range(512)]

for i in range(512):
    for j in range(512):
        if i == j or j == 0:
            comb[i][j] = 1
        else:
            comb[i][j] = (comb[i - 1][j - 1] + comb[i - 1][j]) % BIG_NUMBER

freq = []
sum = [0]            
for c in ascii_lowercase:
    if c in counter.keys():
        freq.append(counter[c])
        sum.append(sum[-1] + counter[c])

n = len(freq)
dp = [[0] * 300 for _ in range(n + 1)]
dp[0][0] = 1

for i in range(n):
    for j in range(sum[i] + 1):
        for k in range(1, freq[i] + 1):
            for l in range(min(j, k) + 1):
                t = comb[freq[i] - 1][k - 1]
                t = (t * comb[j][l]) % BIG_NUMBER
                t = (t * comb[sum[i] - j + 1][k - l]) % BIG_NUMBER
                t = (t * dp[i][j]) % BIG_NUMBER
                dp[i + 1][j - l + freq[i] - k] += t
                dp[i + 1][j - l + freq[i] - k] %= BIG_NUMBER
print(dp[n][0])
