#!/usr/bin/python3
# -*- coding: utf-8 -*-

from itertools import permutations

S = input()

checked = set()

def check_double(s):
    for i in range(len(s) - 1):
        if s[i] == s[i+1]:
            return False
    return True

for s in permutations(S):
    if check_double(s):
        checked.add(s)

print(len(checked) % 1000000007)        
    

    
    

    
        
        

    







