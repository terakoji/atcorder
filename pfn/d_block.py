#!/usr/bin/python3
# -*- coding: utf-8 -*-

A1, B1, C1 = input().split()
A2, B2, C2 = input().split()
A3, B3, C3 = input().split()

A = [A1, A2, A3]
B = [B1, B2, B3]
C = [C1, C2, C3]

#回転したもの反転したものも予め用意
A_r = [A3, A2, A1]
B_r = [B3, B2, B1]
C_r = [C3, C2, C1]

# 90度回転させる
def transpose(block):
    return [block[2][0] + block[1][0] + block[0][0],
            block[2][1] + block[1][1] + block[0][1],
            block[2][2] + block[1][2] + block[0][2]]

all_blocks = [A, A_r,
              transpose(A), transpose(transpose(A)), transpose(transpose(transpose(A))),
              transpose(A_r), transpose(transpose(A_r)), transpose(transpose(transpose(A_r))),
              B, B_r,
              transpose(B), transpose(transpose(B)), transpose(transpose(transpose(B))),
              transpose(B_r), transpose(transpose(B_r)), transpose(transpose(transpose(B_r))),
              C, C_r,
              transpose(C), transpose(transpose(C)), transpose(transpose(transpose(C))),
              transpose(C_r), transpose(transpose(C_r)), transpose(transpose(transpose(C_r)))]

print(10)




