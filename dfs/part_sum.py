#!/usr/bin/python3

import numpy as np

def dfs(i, sum, a):
    if (i == len(a)):
        return sum == k
    if (dfs(i + 1, sum + a[i], a)):
        return True
    if (dfs(i + 1, sum, a)):
        return True
    return False

if __name__=="__main__":
    l = list(map(int, input().split()))
    n = l[0]
    a = l[1:-1]
    k = l[-1]
    print(dfs(0, 0, a))
    
            
    
    
