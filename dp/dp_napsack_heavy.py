#!/usr/bin/python3
# -*- coding: utf-8 -*-


N, W = map(int, input().split())
objects = [tuple(map(int, input().split())) for i in range(N)]

dp = {(0, j):0 for j in range(W + 1)}

for i in range(N):
    for j in range(W + 1):
        if j < objects[i][1]:
            dp[(i + 1, j)] = dp[(i, j)]
        else:
            dp[(i + 1, j)] = max(dp[i, j], dp[i, (j - objects[i][1])] + objects[i][0])

print(dp[(N, W)])


