#!/usr/bin/python3
# -*- coding: utf-8 -*-

u"""
https://beta.atcoder.jp/contests/abc006/tasks/abc006_4
"""

import bisect

n = int(input())
a = [int(input()) for i in range(n)]
INF = n + 1

# 最後がaiの部分増加列の長さ
dp = [INF for i in range(n)]

for i in range(n):
    dp[bisect.bisect_left(dp, a[i])] = a[i]
    # print(dp)

print(n - bisect.bisect_left(dp, INF))

