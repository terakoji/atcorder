#!/usr/bin/python3
# -*- coding: utf-8 -*-

u"""
memo化再起バージョン

Problem Statement
N 問の問題があるコンテストがあり、i 問目の問題の配点は pi 点である。コンテスタントは、この問題の中から何問か解き、解いた問題の配点の合計が得点となる。このコンテストの得点は何通り考えられるか。

Constraints
1≤N≤100
1≤pi≤100

Input Format
入力は以下の形式で標準入力から与えられる。
N
p1p2…pN
Output Format
答えを一行に出力せよ。
"""
N = int(input())
points = list(map(int, input().split()))
scores = []
dp = {}

u"""i個目の問題を現在のスコアから解いた場合"""
def rec(i, score):
    if (i, score) in dp.keys():
        return
    else:
        dp[(i, score)] = True
    if i == N:
        scores.append(score)
    else:
        # 解かない
        rec(i + 1, score)
        # 解く
        rec(i + 1, score + points[i])

rec(0, 0)
print(len(scores))
