#!/usr/bin/python3
# -*- coding: utf-8 -*-

u"""
https://beta.atcoder.jp/contests/maximum-cup-2018/tasks/maximum_cup_2018_d
"""

import sys
#import numpy as np
# from inspect import currentframe

# def chkprint(*args):
#     names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
#     print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)


N, M, L, X = map(int, input().split())
a = list(map(int, input().split()))
INF = 100000

# dp[i][j] i番目で最短いくつの周回ってぴったりjにつけるかを保持
#dp = np.full((N + 1, M), INF, dtype=np.int64)
dp = [[INF for j in range(M)] for i in range(N + 1)]
dp[0][0] = 1

for i in range(1, N + 1):
    for j in range(M):
        # i個目の燃料でいける元場所を特定
        reachable = (j - a[i - 1]) % M
        dp[i][j] = min(dp[i-1][j], dp[i-1][reachable] + (reachable + a[i - 1]) // M)
        #chkprint((i, j))
        #chkprint(dp)
        # この下の分岐はいらなそう
        # if dp[i-1][reachable] == INF and dp[i-i][j] == INF:
        #     dp[i][j] = INF
        # else:
        #     dp[i][j] = min(dp[i-1][j], dp[i-1][reachable] + (reachable + a[i - 1]) // M)
            
if dp[N][L]<= X:
    print('Yes')
else:
    print('No')
