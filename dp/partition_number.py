#!/usr/bin/python3
# -*- coding: utf-8 -*-

n, m, M = map(int, input().split())

dp = [[0 for j in range(n + 1)] for i in range(m + 1)]

dp[0][0] = 1
for i in range(m + 1):
    for j in range(n + 1):
        if j - i >= 0:
            dp[i][j] = (dp[i - 1][j] + dp[i][j - i]) % M
        else:
            dp[i][j] = dp[i - 1][j]

print(dp[m][n])
            
