#!/usr/bin/python3
# -*- coding: utf-8 -*-

N, W = map(int, input().split())
objects = [tuple(map(int, input().split())) for i in range(N)]
dp = {}

u"""i以降をwを超えないように選んだ最大値"""
def rec(i, w):
    res = 0
    if (i, w) in dp.keys():
        res = dp[(i, w)]
    elif i >= N:
        res = 0
    elif w < objects[i][1]:
        res = rec(i + 1, w)
    else:
        res = max(rec(i + 1, w), rec(i + 1, w - objects[i][1]) + objects[i][0])
    dp[(i, w)] = res
    return res

print(rec(0, W))
