#!/usr/bin/python3
# -*- coding: utf-8 -*-


N, W = map(int, input().split())
objects = [tuple(map(int, input().split())) for i in range(N)]

dp = {(N, j):0 for j in range(W + 1)}

for i in reversed(range(N)):
    for j in range(W+1):
        if j < objects[i][1]:
            dp[(i, j)] = dp[(i+1, j)]
        else:
            dp[(i, j)] = max(dp[(i + 1), j], dp[(i + 1), (j - objects[i][1])] + objects[i][0])

print(dp[(0, W)])
                             

