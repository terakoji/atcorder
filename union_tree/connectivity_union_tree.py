#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
https://abc049.contest.atcoder.jp/tasks/arc065_b
'''

import itertools
import sys
import itertools
import operator
import collections

from inspect import currentframe

def chkprint(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

def printerr(value):
    print(value, file=sys.stderr) 

u'''Return all length combination'''
def all_comb(iterable):
    for i in range(0, len(iterable) + 1):
        comb = itertools.combinations(iterable, i)
        for pair in comb:
            yield(pair)


class UnionFind:
    def __init__(self, elems=None):
        class KeyDict(dict):
            def __missing__(self, key):
                self[key] = key
                return key

        self.parent = KeyDict()
        self.rank = collections.defaultdict(int)

        if elems is not None:
            for elem in elems:
                _, _ = self.parent[elem], self.rank[elem]

    def find(self, x):
        if self.parent[x] == x:
            return x
        else:
            self.parent[x] = self.find(self.parent[x])
            return self.parent[x]

    def unite(self, x, y):
        x = self.find(x)
        y = self.find(y)
        if self.rank[x] < self.rank[y]:
            self.parent[x] = y
        else:
            self.parent[y] = x
        if self.rank[x] == self.rank[y]:
            self.rank[x] += 1

    def are_same(self, x, y):
        return self.find(x) == self.find(y)

    def grouper(self):
        roots = [(x, self.find(x_par)) for x, x_par in self.parent.items()]
        root = operator.itemgetter(1)
        for _, group in itertools.groupby(sorted(roots, key=root), root):
            yield [x for x, _ in group]


N, K, L = map(int, input().split())
road_connections = UnionFind(range(1, N + 1))
rail_connections = UnionFind(range(1, N + 1))

for i in range(K):
    (p, q) = map(int, input().split())
    road_connections.unite(p, q)

for i in range(L):
    (r, s) = map(int, input().split())
    rail_connections.unite(r, s)

d = collections.defaultdict(lambda: 0)    
for i in range(1, N + 1):
    p1, p2 = road_connections.find(i), rail_connections.find(i)
    d[(p1, p2)] += 1

ans = [str(d[(road_connections.find(i), rail_connections.find(i))]) for i in range(1, N + 1)]
print(' '.join(ans))

           
             
