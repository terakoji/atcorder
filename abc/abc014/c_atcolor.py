#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
https://beta.atcoder.jp/contests/abc009/tasks/abc014_1
"""

import sys
import math
from inspect import currentframe
import bisect

def debug(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

n = int(input())
ab = [tuple(map(int, input().split())) for _ in range(n)]

# bisectを使うと早そう。
# まずはソートこれで下限が並ぶ
ab.sort() 

max_overrlap = 0

# 自分の上限が他の下限より最もかぶるものを探せばいい
for i in range(len(ab)):
    # indはi個目のアンケートがかぶっている最大の要素
    ind = bisect.bisect_right(ab, (ab[i][1] + 1, 1000002))
    max_overrlap = max(max_overrlap, ind - i)
    
print(max_overrlap)

    


