#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
https://beta.atcoder.jp/contests/abc009/tasks/abc014_1
"""

import sys
import math
from inspect import currentframe
import bisect

def debug(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

N = int(input())
d = [[0 if i == j else float('inf') for i in range(N)] for j in range(N)]
for _ in range(N - 1):
    x, y = map(int, input().split())
    d[x - 1][y - 1] = 1
    d[y - 1][x - 1] = 1

# 全点対最短路問題 + alphaでいける？ ワーシャルフロイドで簡単に計算可能

def warshall_floyd():
    for k in range(N):
        for i in range(N):
            for j in range(N):
                d[i][j] = min(d[i][j], d[i][k] + d[k][j])

warshall_floyd()
Q = int(input())
for i in range(Q):
    a, b = map(int, input().split())
    print(d[a - 1][b - 1] + 1)

                




