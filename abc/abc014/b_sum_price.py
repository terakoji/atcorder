#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
https://beta.atcoder.jp/contests/abc009/tasks/abc014_1
"""

import sys
import math
from inspect import currentframe

def debug(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

n, X = map(int, input().split())
a = list(map(int, input().split()))

sum = 0
for i in range(n):
    if 2**i & X != 0:
        sum += a[i]

print(sum)        





