#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
https://beta.atcoder.jp/contests/abc009/tasks/abc014_1
"""

import sys
import math
from inspect import currentframe
from itertools import accumulate

def debug(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

n = int(input())
ab = [tuple(map(int, input().split())) for _ in range(n)]

l = [0] * 1000002

for a, b in ab:
    l[a] += 1
    l[b + 1] -= 1

print(max(accumulate(l)))    
