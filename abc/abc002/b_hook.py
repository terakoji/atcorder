#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc002/tasks/abc002_2
"""

W = input()
print(''.join(filter(lambda c: c not in 'aiueo', W)))
