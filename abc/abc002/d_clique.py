#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc002/tasks/abc002_4
"""

import itertools
import operator
import collections
import sys
from inspect import currentframe

def chkprint(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg), '???') + ' = ' + repr(arg) for arg in args), file=sys.stderr)

u'''Return all length combination'''
def all_comb(iterable):
    for i in range(0, len(iterable) + 1):
        comb = itertools.combinations(iterable, i)
        for pair in comb:
            yield(pair)

N, M = map(int, input().split())
relations = set([tuple(map(int, input().split())) for _ in range(M)])
chkprint(relations)

# とりあえず全探索
comb = all_comb(range(1, N + 1))
count = 1
for members in comb:
    if len(members) > 1:
        chkprint(set(itertools.combinations(members, 2)))
        if set(itertools.combinations(members, 2)).issubset(relations):
            count = len(members)
            chkprint(count)


print(count)            


