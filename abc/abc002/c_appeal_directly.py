#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc002/tasks/abc002_3
"""

xa, ya, xb, yb, xc, yc = map(int, input().split())

def area_2point_with_origin(p1, p2):
    return abs(p1[0] * p2[1] - p1[1] * p2[0]) / 2.0

# aが原点に移動するように平行移動
new_b = (xb - xa, yb - ya)
new_c = (xc - xa, yc - ya)
print(area_2point_with_origin(new_b, new_c))
