#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc002/tasks/abc002_1
"""

X, Y = map(int, input().split())
print(max(X, Y))
