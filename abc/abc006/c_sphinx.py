#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc006/tasks/abc006_3
"""

import sys
from inspect import currentframe
from collections import deque
sys.setrecursionlimit(100000000)


def debug(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

N, M = map(int, input().split())

# condition = 2 * adults + elders = 4N - M

ans = (-1, -1, -1)
for adults in range(N + 1):
    elders = 4 * N - M - 2 * adults
    babies = N - adults - elders
    if 0 <= elders <= N and 0 <= babies <= N:
        ans = (adults, elders, babies)
        break

print(*ans)


