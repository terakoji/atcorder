#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc006/tasks/abc006_2
"""

import sys
from inspect import currentframe
from collections import deque
sys.setrecursionlimit(100000000)


def debug(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

N = int(input())

def tribonacci(n):
    if (n == 0) or (n == 1):
        return 0
    elif (n==2):
        return 1
    a = deque([0, 0, 1])
    ai = sum(a)
    for _ in range(n - 3):
        a.popleft()
        a.append(ai)
        ai = sum(a)
        ai = ai % 10007
    return ai


print(tribonacci(N - 1))
        
    
    



