#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc001/tasks/abc001_4
"""

from math import floor, ceil
from collections import deque

import sys
from inspect import currentframe

def chkprint(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

N = int(input())
rain_times = [list(map(int, input().split('-'))) for _ in range(N)]

chkprint(rain_times)

def to_min(time):
    return (time // 100) * 60 + time % 100

def to_time(min):
    return (min // 60) * 100 + min % 60

def floor_5min(time):
    return (time // 5) * 5 

def ceil_5min(time):
    return (ceil(time / 5)) * 5 

def merge_time(time1, time2):
    return (min(time1[0], time2[0]), max(time1[1], time2[1]))

def are_intersected(time1, time2):
    return time1[1] >= time2[0]


round_rain_times = [(floor_5min(to_min(time[0])), ceil_5min(to_min(time[1]))) for time in rain_times]
round_rain_times.sort()
combine_time = []
que = deque(round_rain_times)
chkprint(que)
while len(que) != 0:
    start_time = que.popleft()
    while len(que) != 0:
        if are_intersected(start_time, que[0]):
            next_time = que.popleft()
            start_time = merge_time(start_time, next_time)
        else:
            break
    combine_time.append(start_time)

combine_time_full = [(to_time(time[0]), to_time(time[1])) for time in combine_time]
chkprint(combine_time_full)

for time in combine_time_full:
    print('{0:04d}-{1:04d}'.format(time[0], time[1]))
