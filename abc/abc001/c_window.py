#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc001/tasks/abc001_3
"""

Deg, Dis = map(int, input().split())

DIRECTIONS = ['N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW', 'N']
WIND_FORCE_MAX = [0.0, 0.3, 1.6, 3.4, 5.5, 8.0, 10.8, 13.9, 17.2, 20.8, 24.5, 28.5, 32.7, float('inf')]

my_round=lambda x:((x * 10 * 2 + 1) // 2) / 10

direction = DIRECTIONS[round(Deg / 3600.0 * 16.0)]
wind_speed = my_round(Dis / 60.0)
for i in range(13):
    if WIND_FORCE_MAX[i] <= wind_speed < WIND_FORCE_MAX[i + 1]:
        wind_force = i
        break
    
if wind_force == 0:
    direction = 'C'
print(direction, wind_force) 
