#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
https://beta.atcoder.jp/contests/abc054/tasks/abc54_1
"""

import sys
import math
from inspect import currentframe

def debug(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

A, B = map(int, input().split())
if A == 1:
    A = 14
if B == 1:
    B = 14
    
if A > B:
    print('Alice')
elif A < B: 
    print('Bob')
else:
    print('Draw')



