#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
https://beta.atcoder.jp/contests/abc054/tasks/abc54_1
"""

import sys
import math
from inspect import currentframe

def debug_print(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

N, M = map(int, input().split())
A = [input() for _ in range(N)]
B = [input() for _ in range(M)]


delta = N - M
match = False

if delta == 0:
    match = (A==B)
else:
    for dx in range(delta):
        for dy in range(delta):
            window = [pixels[dx:dx+M] for pixels in A[dy:dy+M]]
            if window == B:
                match = True
                break

if match:
    print('Yes')
else:
    print('No')
