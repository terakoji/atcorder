#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc004/tasks/abc004_4
"""

import sys
import math
from inspect import currentframe

def chkprint(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)
    
R, G, B = map(int, input().split())

u"""nのマーブルを左右に制約無しで下ろすの必要な回数
mをバラしたときの短いほうの流さとすると等差数列の和を使い計算んできる"""
def flatten_lr(n):
    m = (n - 1) // 2
    return m ** 2 + m + (m + 1) * ((n - 1) % 2)

print(flatten_lr(R) + flatten_lr(G) + flatten_lr(B))
      
