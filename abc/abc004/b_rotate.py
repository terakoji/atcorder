#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc004/tasks/abc004_1
"""

c = [input() for i in range(4)]

for row in reversed(c):
    print(row[::-1])
