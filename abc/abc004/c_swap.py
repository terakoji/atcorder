#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc004/tasks/abc004_3
"""

from collections import deque

import sys
from inspect import currentframe

def chkprint(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

# Nが5回毎に循環し30回で元に戻る
# まず5で割った回数を出す

N = int(input())

rotation = N // 5
swap = N % 5
# rotate mod 6が循環回数であり一番後ろに来る番号
circulate = rotation % 6
chkprint(circulate)
card = deque(range(1, 7))
chkprint(card)
card.rotate(-circulate)
for i in range(swap):
    card[i], card[i + 1] = card[i + 1], card[i]

print("".join(map(str, card)))
