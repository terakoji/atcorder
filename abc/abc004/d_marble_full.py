#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc004/tasks/abc004_4
"""

import sys
import math
from inspect import currentframe

def chkprint(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)
    
R, G, B = map(int, input().split())

total = R + G + B
inf = float('inf')

R_CENTER, G_CENTER, B_CENTER = 400, 500, 600
BOX_MAX = 1000

# テーブル位置とマーブルの残り総数でdp
dp = [[inf for _ in range(total + 1)] for _ in range(BOX_MAX)]

def move_cost(pos, rest):
    if rest <= R:
        return abs(R_CENTER - pos)
    elif rest <= R + G:
        return abs(G_CENTER - pos)
    else:
        return abs(B_CENTER - pos)

for i in range(BOX_MAX):
    dp[i][0] = 0

for pos in range(1, BOX_MAX):
    for rest in range(1, total + 1):
        dp[pos][rest] = min(dp[pos - 1][rest], dp[pos - 1][rest - 1] + move_cost(pos, rest))
    
print(dp[-1][-1])    

