#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc003/tasks/abc003_2
"""
import re

import sys
from inspect import currentframe

def chkprint(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

S = input()
T = input()

table = str.maketrans({
    'a': '[a@]',
    't': '[t@]',
    'c': '[c@]',
    'o': '[o@]',
    'd': '[d@]',
    'e': '[e@]',
    'r': '[r@]',
    '@': '[atcoder@]',
})

S = S.translate(table)

chkprint(S)
chkprint(T)

print('You can win' if re.fullmatch(S, T) != None else "You will lose")
