#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc003/tasks/abc003_1
"""

N = int(input())

print(sum([i * 10000 for i in range(1, N + 1)]) / N)
