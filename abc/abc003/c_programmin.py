#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc003/tasks/abc003_3
"""
import re

import sys
from inspect import currentframe

def chkprint(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

N, K = map(int, input().split())
R = list(map(int, input().split()))

R.sort()
C = 0
for i in range(K):
    C = (C + R[N - K + i]) / 2

print(C)
