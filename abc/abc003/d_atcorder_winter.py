#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc003/tasks/abc003_4
"""

import sys
import math
from inspect import currentframe

def chkprint(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)
    
R, C = map(int, input().split())
X, Y = map(int, input().split())
D, L = map(int, input().split())

#まず区切りの中の配列を考えるこれは簡単で XYからDを取ってくる組み合わせの数で計算できる
def permutation(n, r):
    return math.factorial(n) // math.factorial(n-r)

def combination(n, r):
    return permutation(n, r) // math.factorial(r)

num_desk_pattern = combination(X * Y, D)
chkprint(num_desk_pattern)

#これに区切りの配置を考える これはもっと簡単
num_separator_pattern = (R - X + 1) * (C - Y + 1)

print((num_separator_pattern * num_desk_pattern) % 1000000007)

