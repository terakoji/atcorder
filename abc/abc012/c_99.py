#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
https://beta.atcoder.jp/contests/abc009/tasks/abc012_2
"""

import sys
import math
from inspect import currentframe

def debug(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

N = int(input())
lack = 2025 - N
for i in range(1, 10):
    for j in range(1, 10):
        if i * j == lack:
            print('{} x {}'.format(i,j))





