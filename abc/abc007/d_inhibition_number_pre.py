#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc007/tasks/abc007_4
"""

import sys
from inspect import currentframe

def debug(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

A, B = map(int, input().split())

count = 0
for i in range(A, B + 1):
    if str(i).find('4') != -1 or str(i).find('9') != -1:
        count += 1

print(count)


