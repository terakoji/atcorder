#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc007/tasks/abc007_4
"""

import sys
from inspect import currentframe

def debug(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

A, B = map(int, input().split())

# 10iまでに含まれるnumber
contain_ten_power = [0] * 19

for i in range(1, 19):
    contain_ten_power[i] = (10 * contain_ten_power[i - 1] + 2 * (10**(i - 1) - contain_ten_power[i - 1]))

# i桁までの含まれる数を計算
def calc_contain(i, x):
    n = int(str(x)[-i])
    if i==1:
        if 0 <= n <= 3:
            return 0
        if 4 <= n <= 8:
            return 1
        if n == 9:
            return 2
    elif 0 <= n <= 3:
        return n * contain_ten_power[i - 1] + calc_contain(i - 1, x)
    elif n == 4:
        num_prev = int(str(x)[-(i - 1):])
        return n * contain_ten_power[i - 1] + num_prev + 1
    elif 5 <= n <= 8:
        return (n - 1) * contain_ten_power[i - 1] + 10 ** (i - 1) + calc_contain(i - 1, x)
    elif n == 9:
        num_prev = int(str(x)[-(i - 1):])
        return (n - 1) * contain_ten_power[i - 1] + 10 ** (i - 1) + num_prev + 1

# 桁数取得
def calc_digit_num(x):
    return len(str(x))

contaion_A = calc_contain(calc_digit_num(A - 1), A - 1)
contaion_B = calc_contain(calc_digit_num(B), B)

print(contaion_B - contaion_A)




