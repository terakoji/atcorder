#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc008/tasks/abc008_2
"""

import sys
from inspect import currentframe
from itertools import permutations
import math

def debug(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)


N = int(input())
C = [int(input()) for _ in range(N)]
expectation_front = 0

for coin in C:
    S = len([True for j in C if coin % j == 0]) - 1
    if S % 2 == 0:
        expectation_front += (S + 2) / (2 * S + 2) 
    else:
        expectation_front += 0.5 
        
print(expectation_front)
