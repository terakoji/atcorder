#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc008/tasks/abc008_2
"""

import sys
from inspect import currentframe
from itertools import permutations
import math

def debug(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

#とりあえず全探索

N = int(input())
C = [int(input()) for _ in range(N)]
coeff = math.factorial(len(C))
debug(coeff)
expectation_front = 0

for coins in permutations(C):
    for i in range(len(coins)):
        expectation_front += len([True for j in range(i) if coins[i] % coins[j] == 0]) % 2 == 0
        
print(expectation_front / coeff)

