#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc008/tasks/abc008_2
"""

import sys
from inspect import currentframe
from collections import Counter

def debug(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

N = int(input())
S = [input() for _ in range(N)]
count = Counter(S)
print(count.most_common()[0][0])

