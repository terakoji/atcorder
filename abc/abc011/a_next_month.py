#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc004/tasks/abc011_1
"""

N = int(input())
print((N % 12) + 1)
