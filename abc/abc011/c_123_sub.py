#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc004/tasks/abc011_3
"""

import sys
from inspect import currentframe

def debug_print(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

N = int(input())
NG1 = int(input())
NG2 = int(input())
NG3 = int(input())

NGs = [NG1, NG2, NG3]

dp = [float('inf')] * (N + 1)

dp[N] = 0

for i in range(N, -1, -1):
    if i in NGs:
        continue
    for j in range(1, 4):
        dp[i - j] = min(dp[i] + 1, dp[i - j])

if dp[0] <= 100 and N not in NGs:
    print('YES')
else:
    print('NO')
    
                           

