#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc004/tasks/abc011_1
"""

S = input()
print(S[0].upper() + S[1:].lower())
