#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc009/tasks/abc011_4
"""

import sys
from math import factorial
from inspect import currentframe
from itertools import combinations_with_replacement

def debug_print(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

N, D = map(int, input().split())
X, Y = map(int, input().split())

# 4項分布を計算(1/4の4事象)
def multi_dist(n, xp, xm, yp, ym):
    fn = factorial(n)
    f1_4 = factorial(xp) * factorial(xm) * factorial(yp) * factorial(ym)
    p1_4 = (0.25) ** (xp + xm + yp + ym)
    return fn / f1_4 * p1_4

# x,yで進むべき回数
if (X % D != 0) or (Y % D != 0) or (X // D) > N or (Y // D) > N:
    print(0)
else:    
    x_num = X // D
    y_num = Y // D

    prob = 0
    for xp in range(max(0, x_num // 2), min(N, (N + x_num) // 2) + 1):
        xm = xp - x_num
        yp = (N - 2 * xp + y_num + x_num) // 2
        ym = yp - y_num
        if (0 <= xm <= N) and (0 <= ym <= N) and (0 <= yp <= N) and (xp+ym+yp+ym == N):
            prob += multi_dist(N, xp, xm, yp, ym)
    print(prob)
        
    
    
