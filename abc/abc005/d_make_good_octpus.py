#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc005/tasks/abc005_4
"""

import sys
from inspect import currentframe

sys.setrecursionlimit(10000000)

def debug(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

N = int(input())
D = [list(map(int, input().split())) for _ in range(N)]
Q = int(input())
P = [int(input()) for _ in range(Q)]

#debug(N)
#debug(D)
#debug(Q)
#debug(P)

"""メモ化再起で行くr, cから初まる高さh, 幅w以下の美味しさの和
範囲を超えるならそれ以下の値
"""
delicious_memo = {}

def calc_delicious(r, c, h, w):
    arg = (r, c, h, w)
    delicious = 0
    if (r, c, h, w) in delicious_memo.keys():
        return delicious_memo[(r, c, h, w)]
    if (r + h) > N:
        delcious = calc_delicious(r, c, N - r, w)
    elif (c + w) > N:
        delcious = calc_delicious(r, c, h, N - c)
    elif h == 1 and w == 1:
        delicious = D[r][c]
    elif h == 1:
        delicious = calc_delicious(r, c, 1, w - 1) + calc_delicious(r, c + w - 1, 1, 1)
    else:
        delicious = calc_delicious(r, c, h - 1, w) + calc_delicious(r + h - 1, c, 1, w)
    delicious_memo[(r, c, h, w)] = delicious
    return delicious

max_p = max(P)
max_hw = {}
for h in range(1, min(max_p + 1, N + 1)):
    for w in range(1, min(max_p // h + 1, N + 1)):
        max_hw[(h, w)] = max([calc_delicious(r, c, h, w) for r in range(N - h + 1) for c in range(N - w + 1)])

#debug(max_hw)        
for p in P:
    print(max([max_hw[(h, w)] for h in range(1, min(p + 1, N + 1)) for w in range(1, min(p + 1, N + 1)) if h * w <= p]))


        
              
