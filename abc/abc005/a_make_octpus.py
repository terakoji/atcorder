#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc005/tasks/abc005_1
"""

x, y = map(int, input().split())
print(y // x)
