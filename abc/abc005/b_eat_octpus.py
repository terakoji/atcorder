#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc005/tasks/abc005_2
"""

N = int(input())
T = [int(input()) for _ in range(N)]

print(min(T))
