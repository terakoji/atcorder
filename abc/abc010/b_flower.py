#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
https://beta.atcoder.jp/contests/abc009/tasks/abc010_1
"""

import sys
import math
from inspect import currentframe

def debug(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

n = int(input())
a = list(map(int, input().split()))

def need_pull(flower):
    pull = 0
    while flower % 3 == 2 or flower % 2 == 0:
        pull += 1
        flower -= 1
    return pull

print(sum([need_pull(flower) for flower in a]))





