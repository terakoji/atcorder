#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
https://beta.atcoder.jp/contests/abc009/tasks/abc010_3
"""

import sys
import math
from inspect import currentframe
from collections import namedtuple

Point = namedtuple('Point', ['x', 'y'])

def debug_print(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)


    
txa, tya, txb, tyb, T, V = map(int, input().split())
n = int(input())
girls = [Point(*map(int, input().split())) for _ in range(n)]
start = Point(txa, tya)
goal = Point(txb, tyb)

def distance(src, dest):
    return math.sqrt((dest.x - src.x)**2 + (dest.y - src.y)**2)

movable_distance = V * T
visited = False

for girl in girls:
    if distance(start, girl) + distance(girl, goal) <= movable_distance:
        visited = True
        break

if visited:
    print('YES')
else:
    print('NO')



