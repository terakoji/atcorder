#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
https://beta.atcoder.jp/contests/abc009/tasks/abc009_4
"""

import sys
import math
from inspect import currentframe
import numpy as np
from collections import deque

def debug(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

K, M = map(int, input().split())
A_orig = deque(list(map(int, input().split())))
A_orig.reverse()
C = np.array(list(map(int, input().split())), dtype=np.uint32)

for n in range(M):
    c_and_a = C & np.array(A_orig, dtype=np.uint32)
    temp = c_and_a[0]
    for i in range(1, K):
        temp = temp^c_and_a[i]
    A_orig.pop()
    A_orig.appendleft(temp)

print(A_orig[0])
    


