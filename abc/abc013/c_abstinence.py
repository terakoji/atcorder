#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
https://beta.atcoder.jp/contests/abc009/tasks/abc013_2
"""

import sys
import math
from inspect import currentframe

def debug_print(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

N, H = map(int, input().split())
A, B, C, D, E = map(int, input().split())

# n日で満腹度がhという状態に至る費用 dp[n][h]
dp = {}
dp[(0, H)] = 0

for n in range(N):
    for h in [key[1] for key in dp.keys() if key[0] == n]:
        # 何も食べない。コストは増えないがお腹が空く
        if (h - E) > 0:
            dp[(n + 1, h - E)] = dp[(n, h)]
        # 普通の食事
        dp[(n + 1, h + B)] = dp[(n, h)] + A
        # 質素の食事
        dp[(n + 1, h + D)] = dp[(n, h)] + C

print(min([item[1] for item in dp.items() if item[0][0]==N]))





