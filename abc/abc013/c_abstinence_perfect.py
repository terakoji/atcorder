#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
https://beta.atcoder.jp/contests/abc009/tasks/abc013_2
"""

import sys
import math
from inspect import currentframe

def debug_print(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

N, H = map(int, input().split())
A, B, C, D, E = map(int, input().split())
min_cost = float('inf')

for X in range(N):
    Y = max(0, math.ceil((((N - X) * E - H - B * X) / (D + E))))
    min_cost = min(min_cost, A * X + C * Y)

print(min_cost)            
