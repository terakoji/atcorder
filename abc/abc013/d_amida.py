#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
https://beta.atcoder.jp/contests/abc009/tasks/abc013_4
"""

import sys
import math
import copy
from inspect import currentframe

def debug_print(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

N, M, D = map(int, input().split())
A = list(map(int, input().split()))

# swap!
x = list(range(1, N + 1))

# これで得られるのは単独あみだswap
def amida(x, A):
    for a in A:
        x[a], x[a - 1] = x[a - 1], x[a]
    return x

def amida_block(x, B):
    y = copy.copy(x)
    for i, b in enumerate(B):
        y[i], y[b - 1] = y[b - 1], y[i]
    return y

simple_amida = amida(x, A)
debug_print(simple_amida)

for i in range(D - 1):
    y = amida_block(x, simple_amida)
    x = copy.copy(y)

debug_print(x)
    
for i in range(1, N + 1):
    print(x.index(i) + 1)
        
        

