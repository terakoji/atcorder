#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
https://beta.atcoder.jp/contests/abc009/tasks/abc013_2
"""

import sys
import math
from inspect import currentframe

def debug(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

a = int(input())
b = int(input())

print(min(abs(b - a), abs(10 - b) + abs(a), abs(b) + abs(10-a)))





