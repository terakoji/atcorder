#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
https://beta.atcoder.jp/contests/abc009/tasks/abc51_2
"""

import sys
import math
from inspect import currentframe

def debug(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

K, S = map(int, input().split())

count = 0
for X in range(0, min(S + 1, K + 1)):
    for Y in range(0, min(S + 1, K + 1)):
        Z = S - X - Y
        if 0 <= Z <= K:
            count += 1
print(count)                







