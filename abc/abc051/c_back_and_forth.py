#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
https://beta.atcoder.jp/contests/abc051/tasks/abc51_c
"""

import sys
import math
from inspect import currentframe

def debug(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

sx, sy, tx, ty = map(int, input().split())

# 例であるような経路が必ず最短になる
dx = tx - sx
dy = ty - sy

s_to_t_1st = 'U' * dy + 'R' * dx
t_to_s_1st = 'D' * dy + 'L' * dx

# 一度目の経路を迂回する経路
s_to_t_2nd = 'L' + 'U' * (dy + 1) + 'R' * (dx + 1) + 'D'
t_to_s_2nd = 'R' + 'D' * (dy + 1) + 'L' * (dx + 1) + 'U'

print(s_to_t_1st + t_to_s_1st + s_to_t_2nd + t_to_s_2nd)







