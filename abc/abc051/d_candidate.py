#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
https://beta.atcoder.jp/contests/abc051/tasks/abc51_d
"""

import sys
import math
import copy
from inspect import currentframe

def debug_print(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args), file=sys.stderr)

N, M = map(int, input().split())

d = [[0 if i == j else float('inf') for i in range(N)] for j in range(N)]
edges = []

for _ in range(M):
    a, b, c = map(int, input().split())
    d[a-1][b-1] = c
    d[b-1][a-1] = c
    edges.append((a,b,c))

graph = copy.deepcopy(d)

# 全点対最短路問題　ワーシャルフロイドで簡単に計算可能

for k in range(N):
    for i in range(N):
        for j in range(N):
            d[i][j] = min(d[i][j], d[i][k] + d[k][j])
    
#debug_print(d)
unused_edge = 0
for edge in edges:
    if edge[2] > d[edge[0] - 1][edge[1] - 1]:
        unused_edge += 1
print(unused_edge)
    
    



