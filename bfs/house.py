#!/usr/bin/python3
# -*- coding: utf-8 -*-

H,W = map(int, input().split())
c = []

for i in range(H):
    c.append(list(input()))


def fill(point, c):
    u"""cのpointから移動できるマスを再起で埋める"""
    if not ((-1 < point[0] < H) and (-1 < point[1] < W)):
        return False
    attr = c[point[0]][point[1]]
    if attr == '.' or attr == 's':
        c[point[0]][point[1]] = '#'
        north = (point[0] - 1, point[1])
        south = (point[0] + 1, point[1])
        east = (point[0], point[1] + 1)
        west = (point[0], point[1] - 1)
        return any([fill(p, c) for p in (north, south, east, west)])
    elif attr == 'g':
        return True
    else:
        return False
    
def search_start(c):
    for i in range(H):
        if 's' in c[i]:
            j = c[i].index('s')
            return (i, j)
    return None
        
start = search_start(c)
if fill(start, c):
    print('Yes')
else:
    print('No')
    
                                                                                                

