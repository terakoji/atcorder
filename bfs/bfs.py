#!/usr/bin/python3
# -*- coding: utf-8 -*-

from collections import deque
import sys

def printerr(value):
    print(value, file=sys.stderr) 

R, C = map(int, input().split())
sy, sx = map(int, input().split())
gy, gx = map(int, input().split())
maze = []
queue = deque()
cost = {}

for i in range(R):
    maze.append(list(input()))

def news(pos):
    return ((pos[0] - 1, pos[1]),
            (pos[0], pos[1] + 1),
            (pos[0], pos[1] - 1),
            (pos[0] + 1, pos[1]))

def valid(pos):
    return maze[pos[0]][pos[1]] == '.'

def bfs():
    start = (sy - 1, sx -1)
    goal = (gy - 1, gx - 1)

    current = start
    queue.append(current)
    dist = 0
    cost[current] = dist

    while current != goal:
        # printerr('queue={}'.format(queue))
        # printerr('cost={}'.format(cost))
        # printerr('\n')
        current = queue.popleft()
        next_pos = [x for x in news(current) if (x not in cost.keys()) and valid(x)]
        for pos in next_pos:
            cost[pos] = cost[current] + 1
        queue.extend(next_pos)
    return cost[current]

print(bfs())
    
