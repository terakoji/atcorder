#!/usr/bin/python3
# -*- coding: utf-8 -*-

from collections import deque
import sys

def printerr(value):
    print(value, file=sys.stderr) 

H, W, N = map(int, input().split())
maze = []

for i in range(H):
    maze.append(list(input()))

def search_char(char):
    for i in range(H):
        if char in maze[i]:
            j = maze[i].index(char)
            return (i, j)
    return None

def news(pos):
    return ((pos[0] - 1, pos[1]),
            (pos[0], pos[1] + 1),
            (pos[0], pos[1] - 1),
            (pos[0] + 1, pos[1]))

def valid(pos):
    if 0 <= pos[0] < H and 0 <= pos[1] < W:
        return maze[pos[0]][pos[1]] != 'X'
    else:
        False

def calc_distance(start_char, goal_char):
    u"""2つのポイントの間のかかる時間をbfsで求める"""
    queue = deque()
    cost = {}
    start = search_char(start_char)
    goal = search_char(goal_char)
    current = start
    cost[current] = 0
    queue.append(current)
    while current != goal:
        #printerr('queue={}'.format(queue))
        #printerr('cost={}'.format(cost))
        #printerr('\n')

        current = queue.popleft()
        next_pos = [x for x in news(current) if (x not in cost.keys()) and valid(x)]
        for pos in next_pos:
            cost[pos] = cost[current] + 1
        queue.extend(next_pos)
    return cost[current]

cheeses = list(map(str, range(1,N + 1)))
start = 'S'
cost = 0

for cheese in cheeses:
    #printerr('search {0} to {1}'.format(start, cheese))
    cost += calc_distance(start, cheese)
    start = cheese

print(cost)
                      
