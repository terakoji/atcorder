#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://code-thanks-festival-2017-open.contest.atcoder.jp/tasks/code_thanks_festival_2017_c
"""

import heapq

N, K = map(int, input().split())
ab_list = [tuple(map(int, input().split())) for i in range(N)]

#for ab in abs:
#    heapq.heappush(time_heap, (ab[0], ab))
heapq.heapify(ab_list)

total_time = 0
for i in range(K):
    a, b = ab_list[0]
    total_time += a
    heapq.heapreplace(ab_list, (a + b, b))

print(total_time)
    
