#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from dijkstra import Dijkstra, Edge
from inspect import currentframe

def chkprint(*args):
    names = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
    print(', '.join(names.get(id(arg),'???')+' = '+repr(arg) for arg in args),
          file=sys.stderr)

test_g = {}
test_g[0] = {Edge(to=1, cost=2),
             Edge(to=2, cost=5)}
test_g[1] = {Edge(to=0, cost=2),
             Edge(to=2, cost=4),
             Edge(to=3, cost=6),
             Edge(to=4, cost=10)}
test_g[2] = {Edge(to=0, cost=5),
             Edge(to=1, cost=4),
             Edge(to=3, cost=2)}
test_g[3] = {Edge(to=1, cost=6),
             Edge(to=2, cost=2),
             Edge(to=5, cost=1)}
test_g[4] = {Edge(to=1, cost=10),
             Edge(to=5, cost=3),
             Edge(to=6, cost=5)}
test_g[5] = {Edge(to=3, cost=1),
             Edge(to=4, cost=3),
             Edge(to=6, cost=9)}
test_g[6] = {Edge(to=4, cost=5),
             Edge(to=5, cost=9)}

dijkstra = Dijkstra(test_g)
d, prev = dijkstra.calc(0)
print(d)
print(prev)

             
