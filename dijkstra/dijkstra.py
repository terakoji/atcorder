#!/usr/bin/python3
# -*- coding: utf-8 -*-

from collections import namedtuple
from heapq import heappop, heappush, heapify, heapreplace
import math

Edge = namedtuple('Edge', ('to', 'cost'))

# edgeは(to, cost)
# edgesは[edge0, edge1, ...]
# graphは {vertex0: edges0, ... } な辞書

class Dijkstra(object):
    def __init__(self, graph):
        self._num_vertexes = len(graph)
        self._graph = graph

    u"""startから初まる最短経路を計算"""
    def calc(self, start):
        heap = []
        prev = [-1] * self._num_vertexes
        dist = [math.inf] * self._num_vertexes
        dist[start] = 0
        heappush(heap, (0, start))

        while len(heap) != 0:
            (min_distance, vertex)  = heappop(heap)
            if dist[vertex] < min_distance:
                continue
            for edge in self._graph[vertex]:
                if dist[edge.to] > dist[vertex] + edge.cost:
                    dist[edge.to] = dist[vertex] + edge.cost
                    prev[edge.to] = vertex
                    heappush(heap, (dist[edge.to], edge.to))
        return dist, prev
                
            
