#!/usr/bin/python3
# -*- coding: utf-8 -*-

u"""
https://beta.atcoder.jp/contests/abc006/tasks/abc006_4
"""
import bisect

n = int(input())
a = list(map(int, input().split()))
INF = n + 1

# 最後がaiの部分増加列の長さ
dp = [INF for i in range(n)]

for i in range(n):
    dp[bisect.bisect_left(dp, a[i])] = a[i]

print(bisect.bisect_left(dp, INF))

