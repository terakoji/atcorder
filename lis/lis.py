#!/usr/bin/python3
# -*- coding: utf-8 -*-

u"""
http://judge.u-aizu.ac.jp/onlinejudge/description.jsp?id=DPL_1_D&lang=jp
"""

n = int(input())
a = [int(input()) for i in range(n)]

# 最後がaiの部分増加列の長さ
dp = [0 for i in range(n)]

for i in range(n):
    dp[i] = 1
    for j in range(i):
        if a[j] < a[i]:
            dp[i] = max(dp[i], dp[j] + 1)

print(max(dp))

