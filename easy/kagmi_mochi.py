#!/usr/bin/python3
# -*- coding: utf-8 -*-

N = int(input())
d = [int(input()) for i in range(N)]

print(len(set(d)))
