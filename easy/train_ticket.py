#!/usr/bin/python3

import itertools

if __name__=='__main__':
    str = input()
    signs_comb = list(itertools.product([1, -1], repeat=3))
    for signs in signs_comb:
        sum = int(str[0])
        for i in range(3):
            sum += signs[i] * int(str[i + 1])
        if sum == 7:
            sign_char = ['+' if i==1 else '-' for i in signs]
            break
    exp = str[0] + sign_char[0] + str[1] + sign_char[1] + str[2] + sign_char[2] + str[3] + '=7'
    print(exp)
