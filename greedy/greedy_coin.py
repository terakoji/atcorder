#!/usr/bin/python3
# -*- coding: utf-8 -*-

payment = int(input())
change = 1000 - payment

coin_kinds = [500, 100, 50, 10, 5, 1]
total_num = 0

for coin_kind in coin_kinds:
    num = change // coin_kind
    change -= coin_kind * num
    total_num += num

print(total_num)    
