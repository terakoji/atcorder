#!/usr/bin/python3
# -*- coding: utf-8 -*-

import re

T = int(input())
S = [input() for i in range(T)]
for s in S:
    print(len(re.findall('tokyo|kyoto', s)))


