#!/usr/bin/python3
# -*- coding: utf-8 -*-

N = int(input())
w = [int(input()) for i in range(N)]

stacks = []
for w_box in w:
    candidates = list(filter(lambda x:x>=w_box, stacks))
    if len(candidates) == 0:
        stacks.append(w_box)
    else:
        ind = stacks.index(min(candidates))
        stacks[ind] = w_box

print(len(stacks))
        



