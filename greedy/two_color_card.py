#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
https://beta.atcoder.jp/contests/abc091/tasks/abc091_b
"""

from collections import defaultdict

rewards = defaultdict(lambda: 0)

N = int(input())
for i in range(N):
    rewards[input()] += 1

M = int(input())
for j in range(M):
    rewards[input()] -= 1

print(max(0, max(rewards.values())))
