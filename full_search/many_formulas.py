#!/usr/bin/python3

import itertools

def all_comb(n):
    comb = []
    for i in range(0, n+1):
        comb.extend(list(itertools.combinations(range(n), i)))
    return comb

if __name__=='__main__':
    str = input()
    comb = all_comb(len(str)-1)
    tail = 0
    sum = 0
    for plus_positions in comb:
        head = 0
        for plus_position in plus_positions:
            num = int(str[head:plus_position+1])
            sum += num
            head = plus_position + 1
        num = int(str[head:])
        sum += num
    print(sum)
